dummy:
	exit 1

update:
	go get -v -t -u ./...

######################
# tool installations #

gotest:
	@go install github.com/rakyll/gotest@v0

golangci-lint:
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1

.golangci.yaml:
	@ln -s project-templates/.golangci.yaml ./

reflex:
	@go install github.com/cespare/reflex@v0

license-checker:
	@go install gitlab.com/bosi/license-checker@latest

#################
# tool commands #

run:
	@printf "### Run Application ###\n"
	@go run .

build:
	GOOS=linux GOARCH=386 GO386=softfloat go build -v

test: gotest
	@if [ -f clear_db.sql ]; then MYSQL_PWD=${DB_PASSWORD} mysql -u${DB_USERNAME} -h${DB_HOSTNAME} ${DB_DATABASE}_test < clear_db.sql; fi
	@printf "### Execute Tests ###\n"
	@${GOPATH}/bin/gotest ./... -count=1

test-ci: gotest
	@${GOPATH}/bin/gotest ./... -v -count=1

test-cover: ## run tests with code coverage
	@go test -v -coverpkg=./... -coverprofile=profile.cov ./...
	@go tool cover -func profile.cov

lint: golangci-lint .golangci.yaml
	@${GOPATH}/bin/golangci-lint run ./...

lint-ci: golangci-lint .golangci.yaml
	@${GOPATH}/bin/golangci-lint run --out-format=code-climate ./...

watch: reflex
	@${GOPATH}/bin/reflex --regex='\.go' --inverse-regex='_test' -d none -s -- make -s run

watch-test: reflex
	@${GOPATH}/bin/reflex --regex='\.go' -d none -- make --silent test

check-licenses: license-checker
	@go get ./...
	@${GOPATH}/bin/license-checker -output=LICENSES-3RD-PARTY

gitlab-pages: check-licenses
	@rm -rf public
	@mkdir public
	@cp LICENSES-3RD-PARTY public/licenses-3rd-party.txt

update-project-templates:
	@rm -rf ./project-templates
	@cp -ar ~/.dotfiles/projects/golang ./project-templates

.gitlab-ci.params.yml:
	cp project-templates/gitlab-ci.params.example.yml .gitlab-ci.params.yml

ytt: ## download ytt into the current dir
	curl -Lo ytt https://github.com/k14s/ytt/releases/download/v0.36.0/ytt-linux-amd64
	chmod +x ytt

gitlab-ci: .gitlab-ci.params.yml ytt ## generate .gitlab-ci.yml based on gitlab-ci.params.yml
	@printf '###############################\n' > .gitlab-ci.yml
	@printf '# This file is auto-generated #\n' >> .gitlab-ci.yml
	@printf '###############################\n' >> .gitlab-ci.yml

	@./ytt --ignore-unknown-comments -f ./project-templates/gitlab-ci.template.yml -f .gitlab-ci.params.yml \
	     | sed 's|  |    |g' \
	     | sed 's|- |  - |g' \
	     | sed 's|^\(\S\)|\n\1|g' >> .gitlab-ci.yml
