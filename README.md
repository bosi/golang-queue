# Queue

Queue provides a simple queue with worker pool and async job execution.

## Installation

`go get gitlab.com/bosi/golang-queue`

## Usage

Please take notice that concurrency is not always a good solution. Go routines are small but they create an overhead and
you should use them only when having long execution times e.g. waiting for a http request or a database query.

The following example shows how this package work but as mentions before: in this particular case an implementation
without an async queue is much faster.

```go
package main

import (
	"fmt"
	queue "gitlab.com/bosi/golang-queue"
)

type QueueJob struct {
	base int

	ch chan int
}

func (qj QueueJob) Run() {
	// right here a more time expensive operation should take place like calling an api
	qj.ch <- qj.base * qj.base
}

func main() {
	q := queue.New(200, false)
	q.Run()
	ch := make(chan int, 200)

	for i := 1; i < 1000; i++ {
		q.AddJob(QueueJob{
			base: i,
			ch:   ch,
		})
	}

	for len(ch) > 0 || !q.IsEmpty() {
		fmt.Printf("squared number: %d \n", <-ch)
	}
}

```
