package queue

import (
	"sync"
	"time"
)

const (
	sleepWaitForFinished = 5 * time.Millisecond
	sleepWhenSleeping    = 50 * time.Millisecond
)

// Job is a runnable entity which can be executed by a queue
type Job interface {
	Run()
}

// PanicHandler is a function which is used when any job execution panics
type PanicHandler func(interface{})

// Queue is a object with all required information for running a queue and executing concurrent jobs
type Queue struct {
	maxWorkers     int
	currentWorkers int

	isRunning bool

	exitWhenEmpty bool

	ph PanicHandler

	jobs []Job

	l  *sync.Mutex
	wg *sync.WaitGroup
}

// Stats is a object containing data about a queue
type Stats struct {
	CurrentWorkers int
	NumJobs        int
	IsRunning      bool
}

type worker struct {
	q   *Queue
	job Job
}

// New creates a new queue
// maxWorkers indicate how many jobs can be executed the same time (does not affect the queue size)
// when exitWhenEmpty is set true the queue will stop when it is empty
func New(maxWorkers int, exitWhenEmpty bool) *Queue {
	return &Queue{
		maxWorkers:     maxWorkers,
		currentWorkers: 0,
		exitWhenEmpty:  exitWhenEmpty,
		jobs:           []Job{},
		l:              &sync.Mutex{},
		wg:             &sync.WaitGroup{},
	}
}

// AddJob adds a runnable job to the queue
func (q *Queue) AddJob(job Job) {
	q.l.Lock()
	q.jobs = append(q.jobs, job)
	q.l.Unlock()
}

// Run starts the queue. During this, jobs will be executed in seperated go routines.
func (q *Queue) Run() {
	q.isRunning = true
	go func() {
		for {
			if q.shouldQuit() {
				q.isRunning = false
				break
			}

			if q.shouldSleep() {
				time.Sleep(sleepWhenSleeping)
				continue
			}

			go q.newWorker().work()
		}
	}()
}

// AddPanicHandler is used to set a custom handler function which is called when the job execution throws a panic
// If you do not set a panic handler the panic will just get caught and the queue will continue running.
func (q *Queue) AddPanicHandler(ph PanicHandler) {
	q.ph = ph
}

// WaitForWorkersFinished can be used to wait for all jobs to be done
func (q *Queue) WaitForWorkersFinished() {
	var isNotDone bool
	for {
		q.l.Lock()
		isNotDone = q.currentWorkers > 0 || len(q.jobs) > 0
		q.l.Unlock()

		if !isNotDone {
			break
		}

		time.Sleep(sleepWaitForFinished)
	}

	q.wg.Wait()
}

// Stats return current data about the queue
func (q *Queue) Stats() Stats {
	q.l.Lock()
	s := Stats{
		IsRunning:      q.isRunning,
		CurrentWorkers: q.currentWorkers,
		NumJobs:        len(q.jobs),
	}
	q.l.Unlock()

	return s
}

// IsEmpty determines if no job is processed and no job is waiting for processing.
func (q *Queue) IsEmpty() bool {
	q.l.Lock()
	empty := q.currentWorkers == 0 && len(q.jobs) == 0
	q.l.Unlock()

	return empty
}

// Flush delete all jobs inside the queue. Please note that jobs already in progress will not be canceled.
func (q *Queue) Flush() {
	q.l.Lock()
	q.jobs = []Job{}
	q.l.Unlock()
}

func (q *Queue) newWorker() worker {
	var j Job

	q.l.Lock()
	j, q.jobs = q.jobs[0], q.jobs[1:]
	q.currentWorkers++
	q.wg.Add(1)
	q.l.Unlock()

	return worker{
		job: j,
		q:   q,
	}
}

func (q *Queue) shouldQuit() bool {
	return q.exitWhenEmpty && q.IsEmpty()
}

func (q *Queue) shouldSleep() bool {
	q.l.Lock()
	nocWorkers := q.currentWorkers
	nocJobs := len(q.jobs)
	q.l.Unlock()

	return nocJobs == 0 || nocWorkers >= q.maxWorkers
}

func (w worker) work() {
	defer func() {
		if r := recover(); r != nil {
			if w.q.ph != nil {
				w.q.ph(r)
			}
		}

		w.q.l.Lock()
		w.q.currentWorkers--
		w.q.wg.Done()
		w.q.l.Unlock()
	}()

	w.job.Run()
}
