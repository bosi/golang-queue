package queue

import (
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"syreclabs.com/go/faker"
)

type TestJob struct {
	Data string
}

func (tj *TestJob) Run() {
	tj.Data = "hello world"
}

type TestJobWithPanic struct {
	Data string
}

func (tjwp TestJobWithPanic) Run() {
	panic("nope")
}

func TestNew(t *testing.T) {
	t.Parallel()

	sn := faker.Number().Between(1, 100)
	n, err := strconv.Atoi(sn)
	assert.Nil(t, err)

	q := New(n, false)
	assert.Equal(t, n, q.maxWorkers)
}

func TestQueue_AddJob(t *testing.T) {
	t.Parallel()

	q := New(5, false)

	j1 := TestJob{Data: faker.Lorem().String()}
	j2 := TestJob{Data: faker.Lorem().String()}

	q.AddJob(&j1)
	q.AddJob(&j2)

	assert.Len(t, q.jobs, 2)
	assert.Equal(t, j1.Data, q.jobs[0].(*TestJob).Data)
	assert.Equal(t, j2.Data, q.jobs[1].(*TestJob).Data)
}

func TestQueue_Run(t *testing.T) {
	t.Run("check with exitWhenEmpty", func(t *testing.T) {
		t.Parallel()

		q := New(5, true)

		tj1 := TestJob{Data: faker.Lorem().String()}
		tj2 := TestJob{Data: faker.Lorem().String()}
		q.AddJob(&tj1)
		q.AddJob(&tj2)
		assert.Len(t, q.jobs, 2)
		assert.False(t, q.isRunning)

		q.l.Lock()
		q.Run()
		assert.True(t, q.isRunning)
		time.Sleep(200 * time.Millisecond)
		assert.Len(t, q.jobs, 2, "check that no job is processed when locked")

		q.l.Unlock()
		q.WaitForWorkersFinished()
		assert.Len(t, q.jobs, 0, "check that all jobs are processed")
		time.Sleep(200 * time.Millisecond)
		assert.False(t, q.isRunning)

		q.AddJob(&TestJob{Data: faker.Lorem().String()})
		time.Sleep(200 * time.Millisecond)
		assert.Len(t, q.jobs, 1, "check that queue was stopped after processing all jobs")

		assert.Equal(t, "hello world", tj1.Data, "check that test job 1 was executed")
		assert.Equal(t, "hello world", tj2.Data, "check that test job 2 was executed")
	})

	t.Run("check without exitWhenEmpty", func(t *testing.T) {
		t.Parallel()

		q := New(5, false)
		q.Run()
		assert.Len(t, q.jobs, 0, "check that all jobs are processed")
		assert.True(t, q.isRunning)

		q.AddJob(&TestJob{Data: faker.Lorem().String()})
		time.Sleep(200 * time.Millisecond)
		assert.Len(t, q.jobs, 0, "check queue is running even after all jobs are processed")
		assert.True(t, q.isRunning)
	})

	t.Run("check that worker panic do not end queue", func(t *testing.T) {
		t.Parallel()

		q := New(5, false)
		q.Run()

		q.AddJob(TestJobWithPanic{})
		time.Sleep(200 * time.Millisecond)
		assert.Len(t, q.jobs, 0, "check queue is running even after all jobs are processed")
		assert.True(t, q.isRunning)
		assert.Equal(t, 0, q.currentWorkers)
	})
}

func TestQueue_Stats(t *testing.T) {
	t.Parallel()

	q := New(42, false)
	q.currentWorkers = 21
	q.isRunning = true
	q.AddJob(&TestJob{})
	q.AddJob(&TestJob{})
	q.AddJob(&TestJob{})

	s := q.Stats()
	assert.Equal(t, 21, s.CurrentWorkers)
	assert.Equal(t, true, s.IsRunning)
	assert.Equal(t, 3, s.NumJobs)
}

func TestQueue_ShouldQuit(t *testing.T) {
	t.Parallel()

	scenarien := []struct {
		nocWorkers    int
		noJobs        int
		exitWhenEmpty bool
		expect        bool
	}{
		{nocWorkers: 0, noJobs: 0, exitWhenEmpty: false, expect: false},
		{nocWorkers: 0, noJobs: 0, exitWhenEmpty: true, expect: true},
		{nocWorkers: 0, noJobs: 5, exitWhenEmpty: true, expect: false},
		{nocWorkers: 5, noJobs: 0, exitWhenEmpty: true, expect: false},
	}

	for _, scenario := range scenarien {
		q := New(50, scenario.exitWhenEmpty)
		q.currentWorkers = scenario.nocWorkers

		for i := 0; i < scenario.noJobs; i++ {
			q.jobs = append(q.jobs, &TestJob{})
		}

		assert.Equal(t, scenario.expect, q.shouldQuit())
	}
}

func TestQueue_ShouldSleep(t *testing.T) {
	t.Parallel()

	scenarien := []struct {
		nocWorkers int
		maxWorkers int
		noJobs     int
		expect     bool
	}{
		{nocWorkers: 0, noJobs: 0, maxWorkers: 0, expect: true},
		{nocWorkers: 5, noJobs: 0, maxWorkers: 10, expect: true},
		{nocWorkers: 5, noJobs: 2, maxWorkers: 10, expect: false},
		{nocWorkers: 9, noJobs: 2, maxWorkers: 10, expect: false},
		{nocWorkers: 10, noJobs: 2, maxWorkers: 10, expect: true},
	}

	for _, scenario := range scenarien {
		q := New(scenario.maxWorkers, false)
		q.currentWorkers = scenario.nocWorkers

		for i := 0; i < scenario.noJobs; i++ {
			q.jobs = append(q.jobs, &TestJob{})
		}

		assert.Equal(t, scenario.expect, q.shouldSleep())
	}
}

func TestQueue_Flush(t *testing.T) {
	t.Parallel()

	q := New(42, false)
	q.currentWorkers = 21
	q.isRunning = true
	q.AddJob(&TestJob{})
	q.AddJob(&TestJob{})
	q.AddJob(&TestJob{})

	assert.Len(t, q.jobs, 3)
	q.Flush()
	assert.Len(t, q.jobs, 0)
}

// #################### BENCHMARKS ########################################

func BenchmarkQueue_AddJob10(b *testing.B) {
	for n := 0; n < b.N; n++ {
		q := New(50, true)
		for i := 0; i < 10; i++ {
			q.AddJob(&TestJob{})
		}
	}
}

func BenchmarkQueue_AddJob100(b *testing.B) {
	for n := 0; n < b.N; n++ {
		q := New(50, true)
		for i := 0; i < 100; i++ {
			q.AddJob(&TestJob{})
		}
	}
}

func BenchmarkWorker_Work100(b *testing.B) {
	q := New(50, true)
	for i := 0; i < 100; i++ {
		q.AddJob(&TestJob{})
	}

	for n := 0; n < b.N; n++ {
		qcopy := q
		qcopy.Run()

		for !qcopy.IsEmpty() {
		}
	}
}
